package com.petrova.hotels.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.petrova.hotels.entity.Hotel;
import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.service.HotelService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/hotels")
@Api(tags = "Hotels management")
@RequiredArgsConstructor
public class HotelController {
    private final HotelService service;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public PagedResult<Hotel> getAll(Pageable pageable) {

        return service.getAll(pageable);
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @PostMapping
    public ResponseEntity<Hotel> create(
            @Valid @RequestBody Hotel entity) {
        return new ResponseEntity<>(service.create(entity), HttpStatus.CREATED);
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER","ROLE_USER"})
    @GetMapping("/{id}")
    public ResponseEntity<Hotel> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Hotel with id of %s was deleted successfully!", id));
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @PutMapping("/{id}")
    public ResponseEntity<Hotel> update(
            @PathVariable int id, @Valid @RequestBody Hotel entity) {

        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Hotel> patchHotel(@PathVariable int id, @RequestBody JsonPatch patch) {
        try {
            Hotel entity = service.getOne(id);
            Hotel patched = applyPatch(patch, entity);
            service.update(patched);
            return ResponseEntity.ok(patched);
        } catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private Hotel applyPatch(
            JsonPatch patch, Hotel target) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, Hotel.class);
    }
}