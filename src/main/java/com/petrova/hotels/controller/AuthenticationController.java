package com.petrova.hotels.controller;

import com.petrova.hotels.entity.JwtAuthenticationResponse;
import com.petrova.hotels.entity.LoginDto;
import com.petrova.hotels.service.AuthService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/v1/auth")
@Api(tags = "Authentication management")
@RequiredArgsConstructor
public class AuthenticationController {

    private AuthService authService;

    @Autowired
    public AuthenticationController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/signin")
    @ResponseStatus(OK)
    public JwtAuthenticationResponse login(@Valid @RequestBody LoginDto loginRequest) {
        return authService.authenticateUser(loginRequest);
    }

}
