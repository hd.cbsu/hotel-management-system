package com.petrova.hotels.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Reservation;
import com.petrova.hotels.service.ReservationService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/reservations")
@Api(tags = "Reservations management")
@RequiredArgsConstructor
@Secured({"ROLE_ADMIN","ROLE_MANAGER","ROLE_USER"})
public class ReservationController {

    private final ReservationService service;
    private ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping
    public PagedResult<Reservation> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @PostMapping
    public ResponseEntity<Reservation> create(
            @Valid @RequestBody Reservation entity) {
        return new ResponseEntity<Reservation> (service.create(entity), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Reservation> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Reservation with id of %s was deleted successfully!", id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Reservation> update(
            @PathVariable int id, @Valid @RequestBody Reservation entity) {

        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Reservation> patchReservation(@PathVariable int id, @RequestBody JsonPatch patch) {
        try {
            Reservation entity = service.getOne(id);
            Reservation patched = applyPatch(patch, entity);
            service.update(patched);
            return ResponseEntity.ok(patched);
        } catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private Reservation applyPatch(
            JsonPatch patch, Reservation target) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, Reservation.class);
    }
}