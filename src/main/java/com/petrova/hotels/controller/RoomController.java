package com.petrova.hotels.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Room;
import com.petrova.hotels.service.RoomService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/rooms")
@Api(tags = "Rooms management")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService service;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Secured({"ROLE_ADMIN","ROLE_MANAGER","ROLE_USER"})
    @GetMapping
    public PagedResult<Room> getAll(Pageable pageable) {
        return service.getAll(pageable);
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @PostMapping
    public ResponseEntity<Room> create(
            @Valid @RequestBody Room entity) {
        return new ResponseEntity<Room> (service.create(entity), HttpStatus.CREATED);
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER","ROLE_USER"})
    @GetMapping("/{id}")
    public ResponseEntity<Room> getOne(@PathVariable int id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        service.deleteById(id);
        return ResponseEntity.ok(String.format("Room with id of %s was deleted successfully!", id));
    }

    @Secured({"ROLE_ADMIN","ROLE_MANAGER"})
    @PutMapping("/{id}")
    public ResponseEntity<Room> update(
            @PathVariable int id, @Valid @RequestBody Room entity) {

        service.getOne(id);
        return ResponseEntity.ok(service.update(entity));
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Room> patchRoom(@PathVariable int id, @RequestBody JsonPatch patch) {
        try {
            Room entity = service.getOne(id);
            Room patched = applyPatch(patch, entity);
            service.update(patched);
            return ResponseEntity.ok(patched);
        } catch (JsonPatchException | JsonProcessingException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private Room applyPatch(
            JsonPatch patch, Room target) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return objectMapper.treeToValue(patched, Room.class);
    }
}