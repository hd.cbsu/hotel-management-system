package com.petrova.hotels.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Set;

@Data
@Entity
@Table(name = "reservation_extras")
public class ReservationExtra {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    private String name;

    private BigDecimal price;

    @JsonIgnoreProperties("extras")
    @ManyToMany(mappedBy = "extras")
    private Set<Reservation> reservations;

}