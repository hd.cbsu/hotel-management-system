package com.petrova.hotels.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class PagedResult<T> {

    private List<T> entities;
    private int currentPage;
    private int TotalPages;
    private long entitiesCount;
}
