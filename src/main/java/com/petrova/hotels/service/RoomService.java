package com.petrova.hotels.service;

import com.petrova.hotels.entity.PagedResult;
import com.petrova.hotels.entity.Room;
import com.petrova.hotels.repository.RoomRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class RoomService {

    private RoomRepository repository;

    public RoomService(RoomRepository repository) {
        this.repository = repository;
    }

    public PagedResult<Room> getAll(Pageable pageable) {
        Page<Room> entities = repository.findAll(pageable);
        return new PagedResult<Room>(
                entities.getContent(),
                pageable.getPageNumber(),
                entities.getTotalPages(),
                entities.getTotalElements());
    }

    public Room getOne(int id) {
        Optional<Room> entity = repository.findById(id);

        if (!entity.isPresent()) {
            throw new EntityNotFoundException(String.format("Room with id of %s was not found!", id));
        }
        return entity.get();
    }

    public Room create(Room entityRequest) {
        try{
            return repository.save(entityRequest);
        }
        catch(Exception ex){
            throw new EntityExistsException();
        }
    }

    public void deleteById(int id) {
        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(String.format("Room with id of %s was not found!", id));
        }
    }

    public Room update(Room entity) {
        try {
            return repository.save(entity);
        }catch(Exception e){
            throw new IllegalArgumentException("Incorrect input data for room.");
        }
    }
}
