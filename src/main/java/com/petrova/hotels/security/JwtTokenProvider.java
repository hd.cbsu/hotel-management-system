package com.petrova.hotels.security;

import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Log4j
public class JwtTokenProvider {

    private String jwtSecret = "Secret key";

    private int jwtExpirationInMs = 360000000;

    public String generateToken(Authentication authentication) {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        Map<String, Object> claims = new HashMap<>();
        List<String> stringRoles =
                userPrincipal.getAuthorities().stream()
                        .map(a -> a.getAuthority())
                        .collect(Collectors.toList());
        claims.put("roles", stringRoles);
        claims.put("id", userPrincipal.getId());

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            throw new JwtException("Invalid token");
        } catch (MalformedJwtException ex) {
            throw new JwtException("Invalid token");
        } catch (ExpiredJwtException ex) {
            throw new JwtException("Invalid token");
        } catch (UnsupportedJwtException ex) {
            throw new JwtException("Invalid token");
        } catch (IllegalArgumentException ex) {
            throw new JwtException("Invalid token");
        }
        finally {
            return false;
        }
    }
}
