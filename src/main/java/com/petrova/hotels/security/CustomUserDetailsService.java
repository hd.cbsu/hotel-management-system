package com.petrova.hotels.security;

import com.petrova.hotels.entity.User;
import com.petrova.hotels.repository.UserRepository;
import javassist.NotFoundException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository repository;

    @Autowired
    public CustomUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @SneakyThrows
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) {
        User user = repository.findByEmail(email);
        if(user==null) {
            throw new NotFoundException("User not found [email: " + email + "]");
        }
        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) throws NotFoundException {
        User user = repository.findById(id).orElseThrow(
                () -> new NotFoundException("User not found [id: " + id + "]")
        );
        return UserPrincipal.create(user);
    }
}
